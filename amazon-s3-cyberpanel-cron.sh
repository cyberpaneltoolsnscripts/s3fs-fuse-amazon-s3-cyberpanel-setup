#!/usr/bin/env bash
## Author: Michael Ramsey
## https://gitlab.com/cyberpaneltoolsnscripts/s3fs-fuse-amazon-s3-cyberpanel-setup
## Setup Amazon S3 remote bucket as a local mount so you can do full backups to amazon s3 server for all accounts.
## How to use.

# Loop to loop through all cyberpanel websites and generate a full backup
for site in $(cyberpanel listWebsitesPretty | grep -Ev 'Domain|+----+'| awk -F'|' '{print $3}'|sed -e 's/^[ \t]*//'); do cyberpanel createBackup --domainName ${site} ; done

